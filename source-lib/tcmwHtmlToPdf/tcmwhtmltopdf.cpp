#include <QDateTime>
#include <QDebug>
#include <QFile>
//#include <QMessageBox>
#include <QPrinter>
//#include <QStandardPaths>
#include <QTextDocument>
//#include <QTextStream>

#include <QString>

#include "tcmwhtmltopdf.h"

char * sayHello() {
    qDebug() << "sayHello from Qt cpp";
    QString hello = "Hello from QString";
    const char * cstr = hello.toStdString().c_str();
    return const_cast<char *>(cstr);
}



//TcmwHtmlToPdf::TcmwHtmlToPdf()
//{
//}

//const char * TcmwHtmlToPdf::sayHello() {
//    qDebug() << "xxx Hello xxx";
//    std::string hello("Hello from QString");
//    const char * cstr = hello.c_str();
//    return cstr;
//}

//void TcmwHtmlToPdf::htmlToPdf(QString htmlContent, QString pathToOutputPdfFile) {

//    // Android QPrinter::PrinterResolution make everything too big
//    // Android QPrinter::HighResolution make everything too small
//    // Android QPrinter::ScreenResolution looks just right !
//    QPrinter printer(QPrinter::ScreenResolution);
//    printer.setOutputFormat(QPrinter::PdfFormat);
//    printer.setPaperSize(QPrinter::A4);
//    printer.setPageOrientation(QPageLayout::Landscape);
//    // printer.setPageMargins(QMarginsF(15, 15, 15, 15));
//    printer.setOutputFileName(pathToOutputPdfFile);


//    // Modify the content
//    QDateTime dateTime =  QDateTime::currentDateTime();
//    QString dateTimeUpdated = dateTime.toString("HH:mm:ss");
//    htmlContent.replace("${dateTime}", dateTimeUpdated);

//    // QSize(2466, 1725)
//    // Returns the page's rectangle in unit;
//    // this is usually smaller than the paperRect() since
//    // the page normally has margins between its borders and the paper.
//    int width = printer.pageRect().size().width();
//    qDebug() << "printer.pageRect().size(): " << printer.pageRect().size();
//    qDebug() << "printer.pageRect() width: " << width;

//    // The page seems a little wider than 2466.
//    // I think margins add another 200 or so.
//    htmlContent.replace("${widthPage}", QString::number(width));

//    QTextDocument doc;
//    doc.setHtml(htmlContent);
//    doc.setPageSize(printer.pageRect().size()); // setPageSize is necessary if you want to hide the page number
//    doc.print(&printer);
//}
