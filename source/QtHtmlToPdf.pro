#-------------------------------------------------
#
# Project created by QtCreator 2018-02-05T08:53:09
#
#-------------------------------------------------

QT       += core gui

QT += printsupport

# // QtWebEngine is >= QT 5.10 and only for Desktop platforms
# QT += webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtHtmlToPdf
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#libtcmwHtmlToPdf.so
INCLUDEPATH += $${PWD}/../source-lib/tcmwHtmlToPdf
LIBS += -L$${PWD}/../source-lib/build-tcmwHtmlToPdf-Android_for_x86_GCC_4_9_Qt_5_10_0_for_Android_x86-Release/android-build/libs/x86

LIBS += -ltcmwHtmlToPdf

# Libs to deploy with app
ANDROID_EXTRA_LIBS += $${PWD}/../source-lib/build-tcmwHtmlToPdf-Android_for_x86_GCC_4_9_Qt_5_10_0_for_Android_x86-Release/android-build/libs/x86/libtcmwHtmlToPdf.so

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    res/resource.qrc

