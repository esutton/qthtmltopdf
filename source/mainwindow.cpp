#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QPrinter>
#include <QStandardPaths>
#include <QTextDocument>
#include <QTextStream>

// QtWebEngine is >= QT 5.10 and only for Desktop platforms
// #include <QWebEnginePage>

#include <QJsonDocument>

#include <tcmwhtmltopdf.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::htmlToPdf(QString htmlContent, QString pathToOutputPdfFile) {

    const char * hello = sayHello();
    qDebug() << "sayHello from cpp lib: " << hello;

    // Android QPrinter::PrinterResolution make everything too big
    // Android QPrinter::HighResolution make everything too small
    // Android QPrinter::ScreenResolution looks just right !
    QPrinter printer(QPrinter::ScreenResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setPageOrientation(QPageLayout::Landscape);
    // printer.setPageMargins(QMarginsF(15, 15, 15, 15));
    printer.setOutputFileName(pathToOutputPdfFile);


    // Modify the content
    QDateTime dateTime =  QDateTime::currentDateTime();
    QString dateTimeUpdated = dateTime.toString("HH:mm:ss");
    htmlContent.replace("${dateTime}", dateTimeUpdated);

    // QSize(2466, 1725)
    // Returns the page's rectangle in unit;
    // this is usually smaller than the paperRect() since
    // the page normally has margins between its borders and the paper.
    int width = printer.pageRect().size().width();
    qDebug() << "printer.pageRect().size(): " << printer.pageRect().size();
    qDebug() << "printer.pageRect() width: " << width;

    // The page seems a little wider than 2466.
    // I think margins add another 200 or so.
    htmlContent.replace("${widthPage}", QString::number(width));

    QTextDocument doc;
    doc.setHtml(htmlContent);
    doc.setPageSize(printer.pageRect().size()); // setPageSize is necessary if you want to hide the page number
    doc.print(&printer);
}

void MainWindow::testHtmlToPdf()
{
    QString outputFileName = "xxx-yyy-zzz.pdf";

#ifdef Q_OS_IOS
    // QStandardPaths::DownloadLocation does not exist on iOS
    QString downloadFolderDefault = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)
            + "/Downloads";
#else
    QString downloadFolderDefault = QStandardPaths::writableLocation(QStandardPaths::DownloadLocation);
#endif

    QDir downloadDirDefault(downloadFolderDefault);
    QString pathToOutputPdfFile = downloadDirDefault.absoluteFilePath(outputFileName);

    QString pathToSource = ":/resource/files/Cow_Creek.html";
    QFile fileSource(pathToSource);
    if (!fileSource.open(QFile::ReadOnly | QFile::Text)) {
        qDebug() << "*** Error file not open";
        return;
    }
    qDebug() << "downloadFolderDefault:\n" << downloadFolderDefault;
    qDebug() << "pathToOutputPdfFile:\n" << pathToOutputPdfFile;

    // http://doc.qt.io/qt-5/richtext-html-subset.html
    QTextStream textStream(&fileSource);
    QString htmlContent = textStream.readAll();

    htmlToPdf(htmlContent, pathToOutputPdfFile);
}

void MainWindow::on_pushButton_clicked()
{
    testHtmlToPdf();
}

